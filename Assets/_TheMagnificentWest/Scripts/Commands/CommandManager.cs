using System;
using System.Collections.Generic;
using TheMagnificentWest.Models;
using TheMagnificentWest.Signals;
using Zenject;

namespace TheMagnificentWest.Commands {
    public class CommandManager : IInitializable, ITickable, IDisposable, ICommandManager {

        [Inject]
        private CommandsModel commandsModel;

        [Inject]
        private SignalBus signalBus;

        private readonly Queue<ICommandData> pendingCommands = new Queue<ICommandData>();

        private ICommandData currentCommandInExecution = null;

        public int PendingCommands => pendingCommands.Count;

        public void Initialize() {
            signalBus.Subscribe<GameOverSignal>(OnGameOver);
        }

        public void Dispose() {
            signalBus.Unsubscribe<GameOverSignal>(OnGameOver);
        }

        private void OnGameOver(GameOverSignal gameOverSignal) {
            pendingCommands.Clear();
            currentCommandInExecution = null;
        }


        public void AddCommand(ICommand command, ICommandModel commandModel) {
            EnqueueCommand(new CommandData(command, commandModel));
        }

        public void AddCommand(ICommandData commandData) {
            EnqueueCommand(commandData);
        }

        private void EnqueueCommand(ICommandData commandData) {
            pendingCommands.Enqueue(commandData);
        }

        private void ExecuteCommand(ICommandData commandData) {
            currentCommandInExecution = commandData;
            commandsModel.Add(commandData);
            commandData.Command.Execute();
        }

        private void DequeueCommand() {
            if (pendingCommands.Count > 0) {
                var commandData = pendingCommands.Dequeue();
                ExecuteCommand(commandData);
            } else {
                currentCommandInExecution = null;
            }
        }

        public void Tick() {
            if (currentCommandInExecution != null && currentCommandInExecution.Command.IsDone) {
                DequeueCommand();
            } else if (currentCommandInExecution == null && pendingCommands.Count > 0) {
                DequeueCommand();
            }
        }

    }
}