﻿using System;
using System.Collections.Generic;
using TheMagnificentWest.Config;
using UnityEngine;
using Zenject;

namespace TheMagnificentWest.UI {
    public class WindowSystem : IWindowSystem {

        [Inject]
        private WindowsSO windowsSo;

        [Inject]
        private WindowView.Factory windowViewFactory;

        private Dictionary<Type, WindowView> openedWindows = new Dictionary<Type, WindowView>();

        public WindowView OpenWindow(Type windowType) {
            var windows = windowsSo.WindowData;
            var window = windowsSo.WindowData.Find(x => x.GetType() == windowType);
            foreach (var windowData in windows) {
                Debug.Log(windowData.GetType());
            }

            if (window == null) {
                throw new Exception($"{windowType} doesnt found");
            }


            var newWindow = windowViewFactory.Create(window);

            var view = newWindow.GetComponent(windowType);
            if (view == null) {
                throw new Exception($"Window type component is missing");
            }

            openedWindows.Add(windowType, newWindow);
            return newWindow;
        }

        public void CloseWindow(Type windowType) {
            if (openedWindows.ContainsKey(windowType)) {
                var window = openedWindows[windowType];
                GameObject.Destroy(window.gameObject);
                openedWindows.Remove(windowType);
            }
        }

        public WindowView Get(Type viewType) {
            if (openedWindows.ContainsKey(viewType)) {
                return openedWindows[viewType];
            }

            return null;
        }

        public WindowView OpenWindow<T>() {
            return OpenWindow(typeof(T));
        }

    }
}