using TheMagnificentWest.Signals;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace TheMagnificentWest.UI {
    public class CommandLogBehaviour : MonoBehaviour {

        [SerializeField]
        private Button logButton;

        [SerializeField]
        private GameObject commandLogsGameObject;

        [SerializeField]
        private CommandLogView logViewTemplate;

        [SerializeField]
        private Transform logViewParentTransform;

        [Inject]
        private SignalBus signalBus;

        private void Awake() {
            logViewTemplate.SetActive(false);
        }

        private void OnEnable() {
            logButton.onClick.AddListener(OnCommandLogClicked);
            signalBus.Subscribe<AddCommandLogSignal>(OnCommandLogged);
        }

        private void OnDisable() {
            logButton.onClick.RemoveListener(OnCommandLogClicked);
            signalBus.Unsubscribe<AddCommandLogSignal>(OnCommandLogged);
        }

        private void OnCommandLogged(AddCommandLogSignal commandLogSignal) {
            var newLogView = AddLogView();
            newLogView.SetView(commandLogSignal.LogText, commandLogSignal.Color);
        }

        private void OnCommandLogClicked() {
            commandLogsGameObject.SetActive(!commandLogsGameObject.activeInHierarchy);
        }

        private CommandLogView AddLogView() {
            var newLogView = Instantiate(logViewTemplate, logViewParentTransform, false);
            newLogView.SetActive(true);
            return newLogView;
        }

    }
}