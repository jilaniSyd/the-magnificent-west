using System;
using TheMagnificentWest.Characters;
using TheMagnificentWest.Models;
using TheMagnificentWest.Signals;
using UnityEngine;
using Zenject;

namespace TheMagnificentWest.Systems {
    public class GameOverSystem : IInitializable, IDisposable {

        [Inject]
        private SpawnedCharactersModel spawnedCharactersModel;

        [Inject]
        private SignalBus signalBus;

        public void Initialize() {
            signalBus.Subscribe<CharacterDeadSignal>(OnCharacterDead);
        }

        public void Dispose() {
            signalBus.Unsubscribe<CharacterDeadSignal>(OnCharacterDead);
        }

        private void OnCharacterDead(CharacterDeadSignal characterDeadSignal) {
            if (characterDeadSignal.CharacterView.CharacterType == CharacterType.Player) {
                spawnedCharactersModel.RemovePlayerCharacter(characterDeadSignal.CharacterView);
            } else {
                spawnedCharactersModel.RemoveEnemyCharacter(characterDeadSignal.CharacterView);
            }


            if (spawnedCharactersModel.EnemyCharacters.Count <= 0) {
                signalBus.Fire(new GameOverSignal(GameTurnType.Player));
            } else if (spawnedCharactersModel.PlayerCharacters.Count <= 0) {
                {
                    signalBus.Fire(new GameOverSignal(GameTurnType.AI));
                }
            }
        }

    }
}