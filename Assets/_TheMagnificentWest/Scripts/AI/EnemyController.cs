using System;
using System.Collections.Generic;
using TheMagnificentWest.Characters;
using TheMagnificentWest.Commands;
using TheMagnificentWest.Models;
using TheMagnificentWest.Signals;
using Zenject;
using UniRx;

namespace TheMagnificentWest.AI {
    public class EnemyController : IInitializable, ITickable, IDisposable {

        [Inject]
        private IAIBehaviour aiBehaviour;

        [Inject]
        private GamePlayModel gamePlayModel;

        [Inject]
        private SpawnedCharactersModel spawnedCharactersModel;

        [Inject]
        private ICommandManager commandManager;

        [Inject]
        private SignalBus signalBus;

        private IDisposable disposable;

        private int pendingCommandsRequestCount = 0;

        private Dictionary<CharacterView, int> characterCommands = new Dictionary<CharacterView, int>();

        public void Initialize() {
            disposable = gamePlayModel.Turn.Subscribe(OnGameTurnChanged);
        }

        public void Dispose() {
            disposable.Dispose();
        }

        public void Tick() {
            if (gamePlayModel.Turn.Value == GameTurnType.AI) {
                if (commandManager.PendingCommands == 0) {
                    if (pendingCommandsRequestCount > 0) {
                        FindMovesForCharacter();
                    } else {
                        signalBus.Fire(new GameTurnChanged(GameTurnType.Player));
                    }
                }
            }
        }

        private void OnGameTurnChanged(GameTurnType gameTurnType) {
            if (gameTurnType == GameTurnType.AI) {
                characterCommands.Clear();
                pendingCommandsRequestCount = spawnedCharactersModel.EnemyCharacters.Count;
                FindMovesForCharacter();
            }
        }

        private void FindMovesForCharacter() {
            var enemiesLeft = spawnedCharactersModel.EnemyCharacters.Keys;
            foreach (var character in enemiesLeft) {
                if (!characterCommands.ContainsKey(character)) {
                    var commands = aiBehaviour.CalculateMoves(character);
                    foreach (var commandData in commands) {
                        commandManager.AddCommand(commandData);
                    }

                    characterCommands.Add(character, commands.Count);
                    pendingCommandsRequestCount--;
                    break;
                }
            }
        }

    }
}