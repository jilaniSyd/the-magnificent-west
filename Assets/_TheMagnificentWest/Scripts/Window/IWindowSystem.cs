using System;

namespace TheMagnificentWest.UI {
    public interface IWindowSystem {

        WindowView OpenWindow<T>();
        WindowView OpenWindow(Type windowType);
        void CloseWindow(Type windowType);
        WindowView Get(Type viewType);

    }
}