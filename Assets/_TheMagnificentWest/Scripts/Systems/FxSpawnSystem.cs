using System;
using System.Diagnostics;
using TheMagnificentWest.Commands;
using TheMagnificentWest.Components;
using TheMagnificentWest.Effects;
using TheMagnificentWest.Models;
using TheMagnificentWest.SO;
using UniRx;
using UnityEngine;
using Zenject;
using Object = UnityEngine.Object;

namespace TheMagnificentWest.Systems {
    public class FxSpawnSystem : IInitializable, IDisposable {

        [Inject]
        private FxSO fxSo;

        [Inject]
        private CommandsModel commandsModel;

        private IDisposable disposable;

        public void Initialize() {
            disposable = commandsModel.CommandStack.ObserveAdd().Subscribe(OnCommandAdded);
        }

        public void Dispose() {
            disposable.Dispose();
        }

        private void OnCommandAdded(CollectionAddEvent<ICommandData> commandData) {
            switch (commandData.Value.Command) {
                case AttackCommand attackCommand:
                    if (commandData.Value.CommandModel is AttackCommandModel attackCommandModel) {
                        SpawnAttackFx(attackCommandModel);
                    }

                    break;
                case MoveToPointCommand moveToPointCommand:
                    if (commandData.Value.CommandModel is MoveToPointCommandModel commandModel) {
                        SpawnMoveFx(commandModel);
                    }

                    break;
            }
        }

        private void SpawnMoveFx(MoveToPointCommandModel commandModel) {
            var character = commandModel.Performer;
            var position = commandModel.MovePoint;
            var spawnedFx = SpawnMoveFxObject(position);
            var collectComponent = spawnedFx.GetComponent<DestroyOnCollect>();
            collectComponent.SetCollisionWith(character);
        }

        private void SpawnAttackFx(AttackCommandModel commandModel) {
            if (commandModel.Performer == null || commandModel.Target == null) {
                return;
            }

            var position1 = commandModel.Performer.transform.position;
            var position2 = commandModel.Target.transform.position;

            var spawnedFx = SpawnAttackFxObject(position1);
            spawnedFx.ShowEffect(position1, position2);
        }

        private GameObject SpawnMoveFxObject(Vector3 position) {
            return Object.Instantiate(fxSo.MovePointFx, position, Quaternion.identity);
        }

        private BulletFxBehaviour SpawnAttackFxObject(Vector3 position) {
            return Object.Instantiate(fxSo.BulletFx, position, Quaternion.identity);
        }

    }
}