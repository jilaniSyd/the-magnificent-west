using TheMagnificentWest.Models;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

namespace TheMagnificentWest.UI {
    public class PauseMenuWindow : WindowView {

        [SerializeField]
        private Button closeButton;

        [SerializeField]
        private Button backToMainMenuButton;

        [SerializeField]
        private Button exitButton;

        [Inject]
        private InputModel inputModel;

        [Inject]
        private IWindowSystem windowSystem;

        private void OnEnable() {
            closeButton.onClick.AddListener(OnBackButton);
            backToMainMenuButton.onClick.AddListener(OnBackToMenu);
            exitButton.onClick.AddListener(OnExit);
            inputModel.Active.Value = false;
        }

        private void OnDisable() {
            closeButton.onClick.RemoveListener(OnBackButton);
            backToMainMenuButton.onClick.RemoveListener(OnBackToMenu);
            exitButton.onClick.RemoveListener(OnExit);
            inputModel.Active.Value = true;
        }

        private void OnBackButton() {
            windowSystem.CloseWindow(GetType());
        }

        private void OnBackToMenu() {
            SceneManager.LoadScene("MainScene");
        }

        private void OnExit() {
            Application.Quit();
        }

    }
}