using System;
using System.Collections.Generic;
using TheMagnificentWest.Characters;
using TheMagnificentWest.Commands;

namespace TheMagnificentWest.Models {
    public class PlayerActionsModel {

        public Dictionary<CharacterView, List<ICommandData>> PlayerActions = new Dictionary<CharacterView, List<ICommandData>>();

        public event Action OnCommandAdded;

        public void AddCommand(CharacterView characterView, ICommandData commandData) {
            if (PlayerActions.ContainsKey(characterView)) {
                var actions = PlayerActions[characterView];
                actions.Add(commandData);
            } else {
                PlayerActions[characterView] = new List<ICommandData> {
                    commandData
                };
            }

            OnCommandAdded?.Invoke();
        }

    }
}