using System;
using System.Collections.Generic;
using TheMagnificentWest.Commands;
using TheMagnificentWest.Models;
using UniRx;
using Zenject;

namespace TheMagnificentWest.Systems {
    public class PlayerActionsManager : IInitializable, IDisposable {

        [Inject]
        private PlayerActionsModel playerActionsModel;

        [Inject]
        private CommandsModel commandsModel;

        private IDisposable disposable;

        public void Initialize() {
            disposable = commandsModel.CommandStack.ObserveAdd().Subscribe(OnCommandAdded);
        }

        public void Dispose() {
            disposable.Dispose();
        }

        private void OnCommandAdded(CollectionAddEvent<ICommandData> commandData) {
            var commandModelData = commandData.Value.CommandModel;
            var actionPerformer = commandModelData.Performer;
            playerActionsModel.AddCommand(actionPerformer, commandData.Value);
        }

    }
}