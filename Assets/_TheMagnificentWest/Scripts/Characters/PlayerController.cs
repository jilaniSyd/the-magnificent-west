using System;
using System.Collections.Generic;
using TheMagnificentWest.Commands;
using TheMagnificentWest.Models;
using TheMagnificentWest.Signals;
using UniRx;
using Zenject;

namespace TheMagnificentWest.Characters {
    public class PlayerController : IInitializable, IDisposable {

        [Inject]
        private SignalBus signalBus;

        [Inject]
        private ICommandManager commandManager;

        [Inject]
        private CharacterSelectionModel selectionModel;

        [Inject]
        private PlayerActionsModel playerActionsModel;

        [Inject]
        private SpawnedCharactersModel spawnedCharactersModel;

        [Inject]
        private GamePlayModel gamePlayModel;

        private IDisposable disposable;

        public void Initialize() {
            disposable = gamePlayModel.Turn.Subscribe(OnGameTurnChanged);
            signalBus.Subscribe<MapClickedSignal>(OnMapClicked);
            signalBus.Subscribe<CharacterSelectedSignal>(OnCharacterSelected);
            playerActionsModel.OnCommandAdded += OnPlayerActionAdded;
        }

        public void Dispose() {
            disposable.Dispose();
            signalBus.Unsubscribe<MapClickedSignal>(OnMapClicked);
            signalBus.Unsubscribe<CharacterSelectedSignal>(OnCharacterSelected);
            playerActionsModel.OnCommandAdded -= OnPlayerActionAdded;
        }

        private void OnPlayerActionAdded() {
            if (CanPlayerFinishTurn()) {
                signalBus.Fire<PlayerTurnEndPossibleSignal>();
            }
        }

        private void OnGameTurnChanged(GameTurnType gameTurnType) {
            if (gameTurnType == GameTurnType.AI) {
                playerActionsModel.PlayerActions.Clear();
                selectionModel.Clear();
                ClearCharacterSelection();
            }
        }

        private void OnMapClicked(MapClickedSignal mapClickedSignal) {
            if (selectionModel.SelectedCharacter == null || selectionModel.SelectedCharacter.Value == null) {
                return;
            }

            var selectedChar = selectionModel.SelectedCharacter.Value;
            if (HasPlayerMadeAMove(selectedChar)) {
                return;
            }

            var moveCommandModel = new MoveToPointCommandModel(selectedChar, mapClickedSignal.Point);
            var command = new MoveToPointCommand(moveCommandModel);
            commandManager.AddCommand(command, moveCommandModel);
        }

        private void OnCharacterSelected(CharacterSelectedSignal characterSelectedSignal) {
            var previousSelectedChar = selectionModel.SelectedCharacter.Value;
            if (previousSelectedChar == null) {
                SelectCharacter(characterSelectedSignal);
            } else {
                var character = characterSelectedSignal.Character;
                if (previousSelectedChar != character && character.CharacterType == CharacterType.Player) {
                    selectionModel.SelectedCharacter.Value = character;
                }

                if (AreMoreActionAvailable()) {
                    SelectCharacter(characterSelectedSignal);
                }
            }
        }

        private void SelectCharacter(CharacterSelectedSignal characterSelectedSignal) {
            if (characterSelectedSignal.Character.CharacterType == CharacterType.Player) {
                selectionModel.SelectedCharacter.Value = characterSelectedSignal.Character;
                SetCharacterSelection(characterSelectedSignal.Character);
            } else {
                selectionModel.EnemySelectedCharacter.Value = characterSelectedSignal.Character;
                var selectedCharacter = selectionModel.SelectedCharacter.Value;
                if (selectionModel != null) {
                    if (HasPlayerAttacked(selectedCharacter)) {
                        return;
                    }

                    var attackCommandModel = new AttackCommandModel(selectedCharacter, characterSelectedSignal.Character, selectedCharacter.UnitConfigData.Damage);
                    var command = new AttackCommand(attackCommandModel);
                    commandManager.AddCommand(command, attackCommandModel);
                }
            }
        }

        private void SetCharacterSelection(CharacterView selection) {
            if (selection.CharacterType == CharacterType.Player) {
                var playerCharacters = spawnedCharactersModel.PlayerCharacters;
                foreach (var characterKeyValue in playerCharacters) {
                    if (characterKeyValue.Key == selection) {
                        selection.ShowSelection(true);
                    } else {
                        characterKeyValue.Key.ShowSelection(false);
                    }
                }
            }
        }

        private void ClearCharacterSelection() {
            var playerCharacters = spawnedCharactersModel.PlayerCharacters;
            foreach (var characterKeyValue in playerCharacters) {
                characterKeyValue.Key.ShowSelection(false);
            }
        }

        private bool HasPlayerMadeAMove(CharacterView selectedCharacter) {
            if (playerActionsModel.PlayerActions.ContainsKey(selectedCharacter)) {
                var data = playerActionsModel.PlayerActions[selectedCharacter];
                foreach (ICommandData commandData in data) {
                    if (commandData.Command is MoveToPointCommand) {
                        return true;
                    }
                }
            }

            return false;
        }

        private bool HasPlayerAttacked(CharacterView selectedCharacter) {
            if (selectedCharacter == null) {
                return false;
            }

            if (playerActionsModel.PlayerActions.ContainsKey(selectedCharacter)) {
                var data = playerActionsModel.PlayerActions[selectedCharacter];
                foreach (ICommandData commandData in data) {
                    if (commandData.Command is AttackCommand) {
                        return true;
                    }
                }
            }

            return false;
        }

        private bool CanPlayerFinishTurn() {
            if (selectionModel.SelectedCharacter == null || selectionModel.SelectedCharacter.Value == null) {
                return false;
            }

            bool isTurnFinished = true;
            var playerCharacters = spawnedCharactersModel.PlayerCharacters;
            foreach (KeyValuePair<CharacterView, int> playerCharacter in playerCharacters) {
                if (!HasPlayerMadeAMove(playerCharacter.Key)) {
                    isTurnFinished = false;
                }
            }

            return isTurnFinished;
        }

        private bool AreMoreActionAvailable() {
            if (selectionModel.SelectedCharacter == null || selectionModel.SelectedCharacter.Value == null) {
                return false;
            }

            bool isActionsLeft = true;
            var selectedCharacter = selectionModel.SelectedCharacter.Value;
            if (playerActionsModel.PlayerActions.ContainsKey(selectedCharacter)) {
                var data = playerActionsModel.PlayerActions[selectedCharacter];
                if (data.Count >= 2) {
                    isActionsLeft = false;
                }
            }

            return isActionsLeft;
        }

    }
}