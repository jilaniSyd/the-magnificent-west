using System;
using TheMagnificentWest.Models;
using TheMagnificentWest.Signals;
using UnityEngine;
using Zenject;

namespace TheMagnificentWest.Systems {
    public class GameTurnManager : IInitializable, IDisposable {

        [Inject]
        private SignalBus signalBus;

        [Inject]
        private GamePlayModel gamePlayModel;

        [Inject]
        private InputModel inputModel;


        public void Initialize() {
            signalBus.Subscribe<GameTurnChanged>(OnGameTurnChanged);
        }


        public void Dispose() {
            signalBus.Unsubscribe<GameTurnChanged>(OnGameTurnChanged);
        }

        private void OnGameTurnChanged(GameTurnChanged gameTurnChanged) {
            gamePlayModel.Turn.Value = gameTurnChanged.GameTurnType;
            inputModel.Active.Value = gameTurnChanged.GameTurnType == GameTurnType.Player;
        }

    }
}