using TheMagnificentWest.Characters;
using UnityEngine;

namespace TheMagnificentWest.Models {
    public struct MoveToPointCommandModel : ICommandModel {

        public CharacterView Performer { get; }
        public Vector3 MovePoint { get; }

        public MoveToPointCommandModel(CharacterView performer, Vector3 movePoint) {
            Performer = performer;
            MovePoint = movePoint;
        }

    }
}