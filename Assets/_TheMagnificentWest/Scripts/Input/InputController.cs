using System;
using TheMagnificentWest.Characters;
using TheMagnificentWest.Models;
using TheMagnificentWest.Signals;
using Zenject;
using UnityEngine;
using UniRx;

namespace TheMagnificentWest {
    public class InputController : IInitializable, IDisposable, ITickable {

        [Inject]
        private InputModel inputModel;

        [Inject]
        private SignalBus signalBus;

        [Inject]
        private CharacterSelectionModel characterSelectionModel;

        private Camera camera;
        private IDisposable disposable;

        private int rayMaxDistance = 100;
        private const string MapTag = "Map";
        private const string CharacterTag = "Character";
        private bool isInputActive = false;

        public void Initialize() {
            camera = Camera.main;
            disposable = inputModel.Active.Subscribe(OnInputChanged);
            isInputActive = inputModel.Active.Value;
        }

        public void Dispose() {
            disposable.Dispose();
        }

        public void Tick() {
            if (!isInputActive) {
                return;
            }

            if (Input.GetKeyDown(KeyCode.Mouse0)) {
                Ray ray = camera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out var hitInfo, rayMaxDistance)) {
                    var hitObject = hitInfo.transform.gameObject;
                    if (hitObject.CompareTag(MapTag)) {
                        signalBus.Fire(new MapClickedSignal(hitInfo.point));
                    } else {
                        if (hitObject.CompareTag(CharacterTag)) {
                            var character = hitObject.GetComponent<CharacterView>();
                            //todo remove or implement the system
                            signalBus.Fire(new CharacterSelectedSignal(character));
                        }
                    }
                }
            }
        }

        private void OnInputChanged(bool active) {
            isInputActive = active;
        }

    }
}