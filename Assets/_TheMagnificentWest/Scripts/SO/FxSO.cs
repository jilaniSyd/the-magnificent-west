using TheMagnificentWest.Effects;
using UnityEngine;

namespace TheMagnificentWest.SO {
    [CreateAssetMenu(fileName = "FxSO", menuName = "TheMagnificentWest/ScriptableObjects/FxSO", order = 1)]
    public class FxSO : ScriptableObject {

        [SerializeField]
        private GameObject deathFx;

        [SerializeField]
        private GameObject movePointFX;

        [SerializeField]
        private BulletFxBehaviour bulletFx;

        public GameObject DeathFx => deathFx;
        public GameObject MovePointFx => movePointFX;
        public BulletFxBehaviour BulletFx => bulletFx;

    }
}