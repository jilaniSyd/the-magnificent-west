using UnityEngine;

namespace TheMagnificentWest.Config {
    [CreateAssetMenu(fileName = "Config", menuName = "TheMagnificentWest/ScriptableObjects/ConfigSO", order = 1)]
    public class ConfigSO : ScriptableObject {

        [SerializeField]
        private int mapSize;
        
        [SerializeField]
        private int enemyMinSize;

        [SerializeField]
        private int enemyMaxSize;

        [SerializeField]
        private int playerMinSize;

        [SerializeField]
        private int playerMaxSize;

        public int MapSize => mapSize;
        public int EnemyMinSize => enemyMinSize;

        public int EnemyMaxSize => enemyMaxSize;

        public int PlayerMinSize => playerMinSize;

        public int PlayerMaxSize => playerMaxSize;

    }
}