using TheMagnificentWest.Models;

namespace TheMagnificentWest.Commands {
    public interface ICommandManager {

        void AddCommand(ICommand command, ICommandModel commandModel);

        void AddCommand(ICommandData commandData);

        int PendingCommands { get; }

    }
}