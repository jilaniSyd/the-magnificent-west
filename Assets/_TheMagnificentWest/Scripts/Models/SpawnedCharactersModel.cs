using System.Collections.Generic;
using TheMagnificentWest.Characters;
using UniRx;

namespace TheMagnificentWest.Models {
    public class SpawnedCharactersModel {

        public ReactiveDictionary<CharacterView, int> PlayerCharacters = new ReactiveDictionary<CharacterView, int>();

        public ReactiveDictionary<CharacterView, int> EnemyCharacters = new ReactiveDictionary<CharacterView, int>();

        public void AddPlayerCharacters(ReactiveDictionary<CharacterView, int> characterViews) {
            foreach (KeyValuePair<CharacterView, int> characterView in characterViews) {
                PlayerCharacters[characterView.Key] = characterView.Value;
            }
        }

        public void AddEnemyCharacters(ReactiveDictionary<CharacterView, int> characterViews) {
            foreach (KeyValuePair<CharacterView, int> characterView in characterViews) {
                EnemyCharacters[characterView.Key] = characterView.Value;
            }
        }

        public void RemovePlayerCharacter(CharacterView characterView) {
            PlayerCharacters.Remove(characterView);
        }

        public void RemoveEnemyCharacter(CharacterView characterView) {
            EnemyCharacters.Remove(characterView);
        }

    }
}