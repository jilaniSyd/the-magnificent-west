using System.Collections.Generic;
using UnityEngine;

namespace TheMagnificentWest.Config {
    [CreateAssetMenu(fileName = "UnitConfigSO", menuName = "TheMagnificentWest/ScriptableObjects/UnitConfigSO")]
    public class UnitConfigSO : ScriptableObject {

        [SerializeField]
        private List<UnitConfigData> unitData;

        public List<UnitConfigData> UnitData => unitData;

#if UNITY_EDITOR
        private void OnValidate() {
            foreach (UnitConfigData unitConfigData in unitData) {
                unitConfigData.SetName(unitConfigData.UnitType.ToString());
            }
        }
#endif

    }
}