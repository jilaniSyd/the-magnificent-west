using System.Collections.Generic;
using TheMagnificentWest.Characters;
using UnityEngine;

namespace TheMagnificentWest.Config {
    [CreateAssetMenu(fileName = "CharacterSO", menuName = "TheMagnificentWest/ScriptableObjects/CharacterSO")]
    public class CharactersSO : ScriptableObject {

        [SerializeField]
        private List<CharacterView> PlayerCharacters;

        [SerializeField]
        private List<CharacterView> EnemyCharacters;

        public List<CharacterView> playerCharacters => PlayerCharacters;
        public List<CharacterView> enemyCharacters => EnemyCharacters;

    }
}