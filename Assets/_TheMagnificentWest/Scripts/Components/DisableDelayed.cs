using System.Collections;
using UnityEngine;

namespace TheMagnificentWest.Components {
    public class DisableDelayed : MonoBehaviour {

        [SerializeField]
        private float delayInSeconds;

        [SerializeField]
        private bool autoDisable = false;

        private float remainingTime = 0;

        private void Awake() {
            remainingTime = delayInSeconds;

            if (autoDisable && remainingTime > 0) {
                StartCoroutine(DisableOnDelay());
            }
        }

        public void ShowEffect() {
            gameObject.SetActive(true);
        }

        public void HideEffect() {
            StopAllCoroutines();
            StartCoroutine(DisableOnDelay());
        }

        private IEnumerator DisableOnDelay() {
            while (remainingTime > 0) {
                remainingTime -= Time.deltaTime;
                yield return null;
            }

            gameObject.SetActive(false);
        }

    }
}