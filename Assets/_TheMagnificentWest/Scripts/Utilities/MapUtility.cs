using System.Collections.Generic;
using TheMagnificentWest.Characters;
using UniRx;
using UnityEngine;

namespace TheMagnificentWest.Utilities {
    public static class MapUtility {

        private const float MinOffsetToCharacter = 3f;

        public static float ConvertRangeToMapUnits(float mapSize, float range) {
            return (range * mapSize) / 100;
        }

        public static bool IsPositionTooClose(Vector3 position, ReactiveDictionary<CharacterView, int> characterViews) {
            foreach (KeyValuePair<CharacterView, int> characterView in characterViews) {
                var targetPosition = characterView.Key.transform.position;
                var isCloseToCharacter = IsCloseToCharacter(position, targetPosition);
                if (isCloseToCharacter) {
                    return true;
                }
            }

            return false;
        }

        public static bool IsPositionTooClose(Vector3 position, ReactiveDictionary<CharacterView, int> characterViews, ReactiveDictionary<CharacterView, int> enemyChars) {
            foreach (KeyValuePair<CharacterView, int> characterView in characterViews) {
                var targetPosition = characterView.Key.transform.position;
                var isCloseToCharacter = IsCloseToCharacter(position, targetPosition);
                if (isCloseToCharacter) {
                    return true;
                }
            }

            foreach (KeyValuePair<CharacterView, int> characterView in enemyChars) {
                var targetPosition = characterView.Key.transform.position;
                var isCloseToCharacter = IsCloseToCharacter(position, targetPosition);
                if (isCloseToCharacter) {
                    return true;
                }
            }

            return false;
        }


        private static bool IsCloseToCharacter(Vector3 position, Vector3 targetPosition) {
            Vector3 offset = targetPosition - position;
            float sqrLen = offset.sqrMagnitude;
            if (sqrLen < MinOffsetToCharacter * MinOffsetToCharacter) {
                return true;
            }

            return false;
        }

    }
}