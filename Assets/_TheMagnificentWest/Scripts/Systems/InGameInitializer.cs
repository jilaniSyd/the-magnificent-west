using System;
using TheMagnificentWest.Signals;
using TheMagnificentWest.UI;
using Zenject;

namespace TheMagnificentWest.Systems {
    public class InGameInitializer : IInitializable, IDisposable {

        [Inject]
        private SignalBus signalBus;

        [Inject]
        private IWindowSystem windowSystem;

        public void Initialize() {
            signalBus.Subscribe<GameStartSignal>(OnInit);
        }

        public void Dispose() {
            signalBus.Unsubscribe<GameStartSignal>(OnInit);
        }

        private void OnInit() {
            windowSystem.OpenWindow<InGameMenuUI>();
        }

    }
}