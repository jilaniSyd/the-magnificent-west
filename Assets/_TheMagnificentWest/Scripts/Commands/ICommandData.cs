using TheMagnificentWest.Models;

namespace TheMagnificentWest.Commands {
    public interface ICommandData {

        ICommand Command { get; }
        ICommandModel CommandModel { get; }

    }
}