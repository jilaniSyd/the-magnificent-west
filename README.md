# The magnificent West
## Turn based Strategy show down 

The magnificent West is a turn based strategy western themed showdown game.

## Features
- Select the character to move or attack
- Attack the character after selection
- Win by killing all the characters of the opponent

## Tech
- [Zenject] - Dependency Injection Framework
- [UniRx] - Reactive extensions for Unity

## Time
- I have spent roughly 3 days of work hours on the implementation.

## Whats next
- Implementation of *Addressables* into the game, so the game doesnt need to address the assets thru scriptable objects.
- Implementation of *Object pool* rather than object instantiation.
- Implementation of varied *AI* behaviours that can be changed from settings 

## Known Bugs
- Currently in the player turn, the commands are processed sequentially and waits until the previous one is done. There is no visual feedback that the command has registered, and when the user clicks multiple times, there could be multiple action commands registered.

