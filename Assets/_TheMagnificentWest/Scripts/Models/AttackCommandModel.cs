using TheMagnificentWest.Characters;

namespace TheMagnificentWest.Models {
    public struct AttackCommandModel : ICommandModel {

        public CharacterView Performer { get; }
        public CharacterView Target { get; }
        public int Damage { get; }

        public AttackCommandModel(CharacterView performer, CharacterView target, int damage) {
            Performer = performer;
            Target = target;
            Damage = damage;
        }

    }
}