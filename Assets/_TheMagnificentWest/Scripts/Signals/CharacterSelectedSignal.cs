using TheMagnificentWest.Characters;

namespace TheMagnificentWest.Signals {
    public struct CharacterSelectedSignal {

        public CharacterView Character { get; }
        
        public CharacterSelectedSignal(CharacterView character) {
            Character = character;
        }

    }
}