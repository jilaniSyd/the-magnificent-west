namespace TheMagnificentWest.Commands {
    public interface ICommand {
        void Execute();
        bool IsDone { get; }
    }
}