using TheMagnificentWest.Characters;
using UnityEngine;

namespace TheMagnificentWest.Components {
    public class DestroyOnCollect : MonoBehaviour {

        private CharacterView collectableView;

        private void OnTriggerEnter(Collider other) {
            var character = other.gameObject.GetComponent<CharacterView>();
            if (collectableView != null && character == collectableView) {
                Destroy(gameObject);
            }
        }

        public void SetCollisionWith(CharacterView characterView) {
            collectableView = characterView;
        }

    }
}