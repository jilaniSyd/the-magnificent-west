using UnityEngine;

namespace TheMagnificentWest.Signals {
    public struct AddCommandLogSignal {

        public string LogText { get; }
        public Color Color { get; }

        public AddCommandLogSignal(string logText, Color color) {
            LogText = logText;
            Color = color;
        }

    }
}