using TheMagnificentWest.Characters;
using UniRx;

namespace TheMagnificentWest.Models {
    public class CharacterSelectionModel {

        public ReactiveProperty<CharacterView> SelectedCharacter = new ReactiveProperty<CharacterView>();
        public ReactiveProperty<CharacterView> EnemySelectedCharacter = new ReactiveProperty<CharacterView>();

        public void Clear() {
            SelectedCharacter.Value = null;
            EnemySelectedCharacter.Value = null;
        }
    }
}