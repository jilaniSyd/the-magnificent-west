using UniRx;

namespace TheMagnificentWest.Commands {
    public class CommandsModel {

        public ReactiveCollection<ICommandData> CommandStack = new ReactiveCollection<ICommandData>();

        public void Add(ICommandData commandData) {
            CommandStack.Add(commandData);
        }

    }
}