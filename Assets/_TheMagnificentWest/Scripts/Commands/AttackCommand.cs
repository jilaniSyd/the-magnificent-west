using TheMagnificentWest.Models;

namespace TheMagnificentWest.Commands {
    public class AttackCommand : ICommand {

        private readonly AttackCommandModel attackCommandModel;
        public bool IsDone { get; private set; }

        public AttackCommand(AttackCommandModel commandModel) {
            attackCommandModel = commandModel;
        }

        public void Execute() {
            var attacker = attackCommandModel.Performer;
            var target = attackCommandModel.Target;
            if (attacker == null || target == null) {
                IsDone = true;
                return;
            }

            var attackerConfig = attacker.UnitConfigData;
            var damage = attackerConfig.Damage;
            target.TakeDamage(damage);
            IsDone = true;
        }

    }
}