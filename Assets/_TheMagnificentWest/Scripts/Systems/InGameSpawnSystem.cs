using System;
using System.Collections.Generic;
using TheMagnificentWest.Characters;
using TheMagnificentWest.Config;
using TheMagnificentWest.Models;
using TheMagnificentWest.Signals;
using TheMagnificentWest.Utilities;
using UniRx;
using UnityEngine;
using UnityEngine.Assertions;
using Zenject;
using Random = UnityEngine.Random;

namespace TheMagnificentWest.Systems {
    public class InGameSpawnSystem : IInitializable, IDisposable {

        [Inject]
        private SignalBus signalBus;

        [Inject]
        private CharactersSO charactersSo;

        [Inject]
        private ConfigSO configSo;

        [Inject]
        private SpawnPointsSO spawnPointsSo;

        [Inject]
        private CharacterView.Factory characterFactory;

        [Inject]
        private SpawnedCharactersModel charactersModel;

        [Inject]
        private UnitConfigSO unitConfigSo;

        private const int MaxTriesToFindAPosition = 20;

        public void Initialize() {
            signalBus.Subscribe<GameStartSignal>(SpawnCharacters);
        }

        public void Dispose() {
            signalBus.Unsubscribe<GameStartSignal>(SpawnCharacters);
        }

        private void SpawnCharacters() {
            SpawnPlayerCharacters();
            SpawnEnemyCharacters();
        }

        private void SpawnEnemyCharacters() {
            var spawnAmount = Random.Range(configSo.EnemyMinSize, configSo.EnemyMaxSize + 1);
            var spawnPoints = spawnPointsSo.EnemySpawnPoints;
            var enemyCharacters = charactersSo.enemyCharacters;

            Assert.AreNotEqual(0, spawnPoints.Count);
            Assert.AreNotEqual(0, enemyCharacters.Count);

            var characters = SpawnCharacter(spawnAmount, spawnPoints, enemyCharacters);
            charactersModel.AddEnemyCharacters(characters);
        }

        private void SpawnPlayerCharacters() {
            var spawnAmount = Random.Range(configSo.PlayerMinSize, configSo.PlayerMaxSize + 1);
            var spawnPoints = spawnPointsSo.PlayerSpawnPoints;
            var playerCharacters = charactersSo.playerCharacters;

            Assert.AreNotEqual(0, spawnPoints.Count);
            Assert.AreNotEqual(0, playerCharacters.Count);

            var characters = SpawnCharacter(spawnAmount, spawnPoints, playerCharacters);
            charactersModel.AddPlayerCharacters(characters);
        }

        private ReactiveDictionary<CharacterView, int> SpawnCharacter(int spawnAmount, List<GameObject> spawnPoints, List<CharacterView> characters) {
            ReactiveDictionary<CharacterView, int> characterViews = new ReactiveDictionary<CharacterView, int>();
            for (int i = 0; i < spawnAmount; i++) {
                Vector3 randomPoint = GetRandomPointFromSpawnRegion(spawnPoints, characterViews);
                var randomEnemyIndex = Random.Range(0, characters.Count);
                var randomEnemy = characters[randomEnemyIndex];
                var spawnedCharacter = characterFactory.Create(randomEnemy);
                spawnedCharacter.transform.position = randomPoint;
                var randomUnitData = GetRandomUnit();
                spawnedCharacter.SetView(randomUnitData);
                characterViews.Add(spawnedCharacter, randomUnitData.Health);
#if UNITY_EDITOR
                spawnedCharacter.name = $"{spawnedCharacter.CharacterType} {i}";
#endif
            }

            return characterViews;
        }

        private Vector3 GetRandomPointFromSpawnRegion(List<GameObject> spawnPoints, ReactiveDictionary<CharacterView, int> characterViews) {
            Vector3 randomPoint = Vector3.zero;
            int validTries = MaxTriesToFindAPosition;
            var spawnIndex = Random.Range(0, spawnPoints.Count);
            var randomSpawnPoint = spawnPoints[spawnIndex];
            var collider = randomSpawnPoint.GetComponent<BoxCollider>();
            while (validTries > 0) {
                randomPoint = GetRandomPosition(collider);
                if (MapUtility.IsPositionTooClose(randomPoint, characterViews)) {
                    validTries--;
                } else {
                    break;
                }
            }

            return randomPoint;
        }

        private Vector3 GetRandomPosition(BoxCollider collider) {
            var size = collider.size;
            var position = collider.transform.position;
            float offsetX = Random.Range(position.x - size.x / 2, position.x + size.x / 2);
            float offsetZ = Random.Range(position.z - size.z / 2, position.z + size.z / 2);
            return new Vector3(offsetX, position.y, offsetZ);
        }

        private UnitConfigData GetRandomUnit() {
            var randomIndex = Random.Range(0, unitConfigSo.UnitData.Count);
            return unitConfigSo.UnitData[randomIndex].ShallowCopy();
        }

    }
}