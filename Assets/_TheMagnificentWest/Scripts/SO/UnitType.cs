namespace TheMagnificentWest.Config {
    public enum UnitType {
        Revolver,
        SixShooter,
        QuickShooter,
        DoubleBarrel,
        SharpShooter,
        Winchester
    }
}