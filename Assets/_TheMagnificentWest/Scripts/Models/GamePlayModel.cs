using UniRx;

namespace TheMagnificentWest.Models {
    public class GamePlayModel {

        public ReactiveProperty<GameTurnType> Turn = new ReactiveProperty<GameTurnType>(GameTurnType.Player);

    }

    public enum GameTurnType {

        Player,
        AI

    }
}