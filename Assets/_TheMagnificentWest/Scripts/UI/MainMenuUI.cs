using TheMagnificentWest.Models;
using TheMagnificentWest.Signals;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace TheMagnificentWest.UI {
    public class MainMenuUI : WindowView {

        [SerializeField]
        private Button playButton;

        [SerializeField]
        private Button quitButton;

        //Move to system
        [Inject]
        private InputModel inputModel;

        [Inject]
        private SignalBus signalBus;
        
        private void OnEnable() {
            playButton.onClick.AddListener(OnPlay);
            quitButton.onClick.AddListener(OnQuit);
        }

        private void OnDisable() {
            playButton.onClick.RemoveListener(OnPlay);
            quitButton.onClick.RemoveListener(OnQuit);
        }

        private void OnPlay() {
            gameObject.SetActive(false);
            signalBus.Fire<GameStartSignal>();
            inputModel.Active.Value = true;
        }

        private void OnQuit() {
            Application.Quit();
        }

    }
}