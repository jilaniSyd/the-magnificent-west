using System;
using UnityEngine;

namespace TheMagnificentWest.Config {
    [Serializable]
    public class UnitConfigData {

        [SerializeField]
        private UnitType unitType;

        [SerializeField]
        private string name;

        [SerializeField]
        private int range;

        [SerializeField]
        private int health;

        [SerializeField]
        private int maxHealth;

        [SerializeField]
        private int damage;

        public UnitType UnitType => unitType;
        public int Range => range;
        public int Health => health;
        public int MaxHealth => maxHealth;
        public int Damage => damage;
        public string Name => name;

#if UNITY_EDITOR
        public void SetName(string name) {
            this.name = name;
        }
#endif

        public UnitConfigData ShallowCopy() {
            return (UnitConfigData) MemberwiseClone();
        }

    }
}