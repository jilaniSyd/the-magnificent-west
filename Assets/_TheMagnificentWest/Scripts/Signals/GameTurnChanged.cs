using TheMagnificentWest.Models;

namespace TheMagnificentWest.Signals {
    public struct GameTurnChanged {

        public GameTurnType GameTurnType { get; } 
        public GameTurnChanged(GameTurnType gameTurnType) {
            GameTurnType = gameTurnType;
        }

    }
}