using System;
using System.Collections;
using TheMagnificentWest.Components;
using TheMagnificentWest.Config;
using TheMagnificentWest.Signals;
using TheMagnificentWest.SO;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using Zenject;

namespace TheMagnificentWest.Characters {
    [RequireComponent(typeof(NavMeshAgent))]
    public class CharacterView : MonoBehaviour {

        [SerializeField]
        private NavMeshAgent navMeshAgent;

        [SerializeField]
        private GameObject selectionRing;

        [SerializeField]
        private TextMeshProUGUI nameText;

        [SerializeField]
        private DamageIndicator damageIndicator;

        [SerializeField]
        private Slider healthSlider;

        [SerializeField]
        private CharacterType characterType;

        [SerializeField]
        private DisableDelayed bloodFxGameObject;

        [Inject]
        private SignalBus signalBus;

        [Inject]
        private FxSO fxSo;

        private int currentHealth;
        private UnitConfigData unitConfigData;
        public UnitConfigData UnitConfigData => unitConfigData;
        public CharacterType CharacterType => characterType;
        public string UnitName => unitConfigData.Name;

        private Action onDestinationReached;

        public void ShowSelection(bool isSelected) {
            selectionRing.SetActive(isSelected);
        }

        public void SetView(UnitConfigData unitData) {
            unitConfigData = unitData;
            nameText.text = unitData.Name;
            UpdateHealth(unitData.Health);
        }

        private void UpdateHealth(int health) {
            healthSlider.value = (float) health / 100;
            currentHealth = health;
        }

        public void TakeDamage(int damage) {
            damageIndicator.ShowDamage(damage);
            UpdateHealth(currentHealth - damage);
            bloodFxGameObject.ShowEffect();
            if (currentHealth <= 0) {
                signalBus.Fire(new CharacterDeadSignal(this));
                SpawnDeathFx(transform.position);
                Destroy(gameObject);
            }
        }

        public void MoveToTargetPoint(Vector3 target, Action OnReachedDestination) {
            navMeshAgent.SetDestination(target);
            onDestinationReached = OnReachedDestination;
            StartCoroutine(CheckDestinationReached());
        }

        private IEnumerator CheckDestinationReached() {
            yield return null;
            while (true) {
                if (!navMeshAgent.pathPending) {
                    if (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance) {
                        if (!navMeshAgent.hasPath || navMeshAgent.velocity.sqrMagnitude == 0f) {
                            onDestinationReached?.Invoke();
                            yield break;
                        }
                    }
                }

                yield return new WaitForSeconds(1);
            }
        }

        private void SpawnDeathFx(Vector3 transformPosition) {
            var deathFx = Instantiate(fxSo.DeathFx, transformPosition, Quaternion.identity);
            Destroy(deathFx, 2f);
        }

        public class Factory : PlaceholderFactory<CharacterView, CharacterView> {

        }

    }
}