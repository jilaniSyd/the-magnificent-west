using TheMagnificentWest.Config;
using TheMagnificentWest.UI;
using Zenject;

namespace TheMagnificentWest.Systems {
    public class GameInitializerSystem : IInitializable {

        [Inject]
        private WindowsSO windowsSo;

        [Inject]
        private IWindowSystem windowSystem;

        public void Initialize() {
            var initialWindowsToShow = windowsSo.InitialWindows;
            foreach (var view in initialWindowsToShow) {
                var viewType = view.GetComponent<WindowView>();
                windowSystem.OpenWindow(viewType.GetType());
            }
        }

    }
}