using TMPro;
using UnityEngine;

namespace TheMagnificentWest.UI {
    public class CommandLogView : MonoBehaviour {

        [SerializeField]
        private TextMeshProUGUI logText;

        public void SetView(string log, Color color) {
            logText.text = log;
            logText.color = color;
        }

        public void SetActive(bool isActive) {
            gameObject.SetActive(isActive);
        }


    }
}