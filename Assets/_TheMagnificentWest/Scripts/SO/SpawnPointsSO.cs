using System.Collections.Generic;
using UnityEngine;

namespace TheMagnificentWest.Systems {
    [CreateAssetMenu(fileName = "SpawnPointsSO", menuName = "TheMagnificentWest/ScriptableObjects/SpawnPointsSO")]
    public class SpawnPointsSO : ScriptableObject {

        [SerializeField]
        private List<GameObject> playerSpawnPoints;

        [SerializeField]
        private List<GameObject> enemySpawnPoints;

        public List<GameObject> PlayerSpawnPoints => playerSpawnPoints;
        public List<GameObject> EnemySpawnPoints => enemySpawnPoints;

    }
}