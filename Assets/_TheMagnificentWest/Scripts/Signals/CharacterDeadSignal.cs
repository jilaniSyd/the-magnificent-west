using TheMagnificentWest.Characters;

namespace TheMagnificentWest.Signals {
    public struct CharacterDeadSignal {
        public CharacterView CharacterView { get; }

        public CharacterDeadSignal(CharacterView characterView) {
            CharacterView = characterView;
        }

    }
}