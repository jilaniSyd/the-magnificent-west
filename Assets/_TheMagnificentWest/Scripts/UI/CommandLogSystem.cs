using System;
using TheMagnificentWest.Characters;
using TheMagnificentWest.Commands;
using TheMagnificentWest.Models;
using TheMagnificentWest.Signals;
using Zenject;
using UniRx;
using UnityEngine;

namespace TheMagnificentWest.UI {
    public class CommandLogSystem : IInitializable, IDisposable {

        [Inject]
        private CommandsModel commandsModel;

        [Inject]
        private SignalBus signalBus;

        private IDisposable disposable;

        public void Initialize() {
            disposable = commandsModel.CommandStack.ObserveAdd().Subscribe(OnCommandAdded);
        }

        public void Dispose() {
            disposable.Dispose();
        }

        private void OnCommandAdded(CollectionAddEvent<ICommandData> collectionAddEvent) {
            var commandData = collectionAddEvent.Value;
            switch (commandData.Command) {
                case AttackCommand attackCommand:
                    if (commandData.CommandModel is AttackCommandModel attackCommandModel) {
                        var logText = $" - {attackCommandModel.Performer.UnitName} inflicted {attackCommandModel.Target.UnitName} {attackCommandModel.Damage} damage";
                        Color color = attackCommandModel.Performer.CharacterType == CharacterType.Player ? Color.green : Color.red;
                        signalBus.Fire(new AddCommandLogSignal(logText, color));
                    }

                    break;

                case MoveToPointCommand moveToPointCommand:
                    if (commandData.CommandModel is MoveToPointCommandModel commandModel) {
                        var logText = $" - {commandModel.Performer.UnitName} moved to position at {commandModel.MovePoint}";
                        Color color = commandModel.Performer.CharacterType == CharacterType.Player ? Color.green : Color.red;
                        signalBus.Fire(new AddCommandLogSignal(logText, color));
                    }

                    break;
            }
        }

    }
}