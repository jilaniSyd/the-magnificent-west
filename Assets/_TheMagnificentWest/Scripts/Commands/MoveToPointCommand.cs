using TheMagnificentWest.Models;

namespace TheMagnificentWest.Commands {
    public class MoveToPointCommand : ICommand {

        private readonly MoveToPointCommandModel moveToPointCommandModel;

        public bool IsDone { get; private set; }

        public MoveToPointCommand(MoveToPointCommandModel model) {
            moveToPointCommandModel = model;
            IsDone = false;
        }

        public void Execute() {
            var character = moveToPointCommandModel.Performer;
            if (character == null) {
                return;
            }
            character.MoveToTargetPoint(moveToPointCommandModel.MovePoint, OnDestinationReached);
        }

        private void OnDestinationReached() {
            IsDone = true;
        }
       
    }
}