using System.Collections.Generic;
using TheMagnificentWest.Characters;
using TheMagnificentWest.Commands;
using TheMagnificentWest.Config;
using TheMagnificentWest.Models;
using TheMagnificentWest.Utilities;
using UnityEngine;
using Zenject;

namespace TheMagnificentWest.AI {
    public class AlwaysAttackAIBehaviour : IAIBehaviour {

        [Inject]
        private SpawnedCharactersModel charactersModels;

        [Inject]
        private ConfigSO gameConfigSo;

        private const float MinCharacterSize = 1f;

        public List<ICommandData> CalculateMoves(CharacterView characterView) {
            var commands = new List<ICommandData>();

            var range = characterView.UnitConfigData.Range;
            var convertedRange = MapUtility.ConvertRangeToMapUnits(gameConfigSo.MapSize, range);
            var hasClosestEnemy = TryFindClosestEnemy(characterView.transform.position, convertedRange, out var closestCharacter);
            if (hasClosestEnemy) {
                Vector3 randomPosition; 
                ICommandData moveCommandData = GetMoveCommand(characterView, closestCharacter,out randomPosition);
                if (moveCommandData != null) {
                    commands.Add(moveCommandData);
                }

                var randomAttack = Random.Range(0, 2) == 1;
                if (randomAttack) {
                    ICommandData attackCommand = GetAttackCommand(characterView,randomPosition );
                    if (attackCommand != null) {
                        commands.Add(attackCommand);
                    }
                }
            }

            return commands;
        }

        private ICommandData GetMoveCommand(CharacterView characterView, CharacterView closestEnemy,out Vector3 randomPosition) {
            if (TryGetRandomPositionInCircle(characterView, closestEnemy, out randomPosition)) {
                var moveCommandModel = new MoveToPointCommandModel(characterView, randomPosition);
                var command = new MoveToPointCommand(moveCommandModel);
                return new CommandData(command, moveCommandModel);
            }

            return null;
        }

        private bool TryGetRandomPositionInCircle(CharacterView characterView, CharacterView enemyCharacter, out Vector3 randomPosition) {
            var position = characterView.transform.position;
            var enemyPosition = enemyCharacter.transform.position;
            var distance = Vector3.Distance(position, enemyPosition);
            var range = characterView.UnitConfigData.Range;
            var convertedRange = MapUtility.ConvertRangeToMapUnits(gameConfigSo.MapSize, range);
            var radius = convertedRange;
            if (distance < convertedRange) {
                radius = distance;
            }

            var mapSizeRange = (float) gameConfigSo.MapSize / 2 - MinCharacterSize;
            var mapSize = new Vector2(-mapSizeRange, mapSizeRange);
            int numberOfTries = 10;
            while (numberOfTries > 0) {
                var randomPointInCircle = Random.insideUnitCircle * radius;
                var randomPointAround = new Vector3(position.x + randomPointInCircle.x, position.y, position.z + randomPointInCircle.y);
                if (randomPointAround.x > mapSize.x &&
                    randomPointAround.x < mapSize.y &&
                    randomPointAround.z > mapSize.x &&
                    randomPointAround.z < mapSize.y) {
                    if (!MapUtility.IsPositionTooClose(randomPointAround, charactersModels.EnemyCharacters, charactersModels.PlayerCharacters)) {
                        randomPosition = randomPointAround;
                        return true;
                    }
                }

                numberOfTries--;
            }

            randomPosition = Vector3.zero;
            return false;
        }
 
        private ICommandData GetAttackCommand(CharacterView characterView, Vector3 targetPosition) {
            var range = characterView.UnitConfigData.Range;
            var convertedRange = MapUtility.ConvertRangeToMapUnits(gameConfigSo.MapSize, range);
            var hasClosestEnemy = TryFindClosestEnemy(targetPosition, convertedRange, out var closestCharacter);
            if (hasClosestEnemy) {
                var attackCommandModel = new AttackCommandModel(characterView, closestCharacter, characterView.UnitConfigData.Damage);
                var attackCommand = new AttackCommand(attackCommandModel);
                ICommandData commandData = new CommandData(attackCommand, attackCommandModel);
                return commandData;
            }

            return null;
        }

        private bool TryFindClosestEnemy(Vector3 characterPosition, float convertedRange, out CharacterView closestCharacter) {
            var playerCharacters = charactersModels.PlayerCharacters.Keys;
            float closestDistance = 100;
            bool foundEnemy = false;
            closestCharacter = null;
            foreach (CharacterView characterView in playerCharacters) {
                var playerPosition = characterView.transform.position;
                var distance = Vector3.Distance(characterPosition, playerPosition);
                if (distance < closestDistance) {
                    closestDistance = distance;
                    closestCharacter = characterView;
                    foundEnemy = true;
                }
            }

            return foundEnemy;
        }

    }
}