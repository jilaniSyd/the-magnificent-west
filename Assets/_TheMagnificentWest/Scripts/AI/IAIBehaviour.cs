using System.Collections.Generic;
using TheMagnificentWest.Characters;
using TheMagnificentWest.Commands;

namespace TheMagnificentWest.AI {
    public interface IAIBehaviour {

        List<ICommandData> CalculateMoves(CharacterView characterView);

    }
}