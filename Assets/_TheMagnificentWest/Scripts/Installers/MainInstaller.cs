using TheMagnificentWest.AI;
using TheMagnificentWest.Characters;
using TheMagnificentWest.Commands;
using TheMagnificentWest.Config;
using TheMagnificentWest.Models;
using TheMagnificentWest.Signals;
using TheMagnificentWest.SO;
using TheMagnificentWest.Systems;
using TheMagnificentWest.UI;
using UnityEngine;
using Zenject;

namespace TheMagnificentWest.Installers {
    public class MainInstaller : MonoInstaller {

        [SerializeField]
        private ConfigSO configSo;

        [SerializeField]
        private WindowsSO windowsSo;

        [SerializeField]
        private CharactersSO charactersSo;

        [SerializeField]
        private UnitConfigSO unitConfigSo;

        [SerializeField]
        private SpawnPointsSO spawnPointsSo;

        [SerializeField]
        private FxSO fxSo;

        public override void InstallBindings() {
            BindConfig();
            BindViews();
            BindModels();
            BindSignals();
            BindControllers();
        }

        private void BindViews() {
            Container.BindFactory<WindowView, WindowView, WindowView.Factory>().FromSubContainerResolve().ByMethod(CreateWindow);
            Container.BindFactory<CharacterView, CharacterView, CharacterView.Factory>().FromSubContainerResolve().ByMethod(CreateCharacter);
        }

        private void CreateCharacter(DiContainer subContainer, CharacterView characterView) {
            subContainer.Bind<CharacterView>().FromComponentInNewPrefab(characterView).AsTransient();
        }

        private void CreateWindow(DiContainer subContainer, WindowView view) {
            subContainer.Bind<WindowView>().FromComponentInNewPrefab(view).AsTransient();
        }

        private void BindModels() {
            Container.Bind<InputModel>().AsSingle().NonLazy();
            Container.Bind<GamePlayModel>().AsSingle().NonLazy();
            Container.Bind<SpawnedCharactersModel>().AsSingle().NonLazy();
            Container.Bind<CharacterSelectionModel>().AsSingle().NonLazy();
            Container.Bind<MoveToPointCommandModel>().AsSingle();
            Container.Bind<AttackCommandModel>().AsSingle();
            Container.Bind<CommandsModel>().AsSingle();
            Container.Bind<PlayerActionsModel>().AsSingle();
        }

        private void BindConfig() {
            Container.Bind<ConfigSO>().FromScriptableObject(configSo).AsSingle().NonLazy();
            Container.Bind<WindowsSO>().FromScriptableObject(windowsSo).AsSingle().NonLazy();
            Container.Bind<CharactersSO>().FromScriptableObject(charactersSo).AsSingle().NonLazy();
            Container.Bind<UnitConfigSO>().FromScriptableObject(unitConfigSo).AsSingle().NonLazy();
            Container.Bind<SpawnPointsSO>().FromScriptableObject(spawnPointsSo).AsSingle().NonLazy();
            Container.Bind<FxSO>().FromScriptableObject(fxSo).AsSingle().NonLazy();
        }

        private void BindControllers() {
            Container.BindInterfacesTo<WindowSystem>().AsSingle().NonLazy();
            Container.BindInterfacesTo<GameInitializerSystem>().AsSingle().NonLazy();
            Container.BindInterfacesTo<CommandLogSystem>().AsSingle().NonLazy();
            Container.BindInterfacesTo<InputController>().AsSingle().NonLazy();
            Container.BindInterfacesTo<InGameSpawnSystem>().AsSingle().NonLazy();
            Container.BindInterfacesTo<InGameInitializer>().AsSingle().NonLazy();
            Container.BindInterfacesTo<PlayerController>().AsSingle().NonLazy();
            Container.BindInterfacesTo<EnemyController>().AsSingle().NonLazy();
            Container.BindInterfacesTo<AlwaysAttackAIBehaviour>().AsSingle().NonLazy();
            Container.BindInterfacesTo<CommandManager>().AsSingle().NonLazy();
            Container.BindInterfacesTo<PlayerActionsManager>().AsSingle().NonLazy();
            Container.BindInterfacesTo<GameTurnManager>().AsSingle().NonLazy();
            Container.BindInterfacesTo<GameOverSystem>().AsSingle().NonLazy();
            Container.BindInterfacesTo<FxSpawnSystem>().AsSingle().NonLazy();
        }

        private void BindSignals() {
            SignalBusInstaller.Install(Container);
            Container.DeclareSignal<GameStartSignal>();
            Container.DeclareSignal<MapClickedSignal>();
            Container.DeclareSignal<CharacterSelectedSignal>();
            Container.DeclareSignal<AddCommandLogSignal>();
            Container.DeclareSignal<GameTurnChanged>();
            Container.DeclareSignal<CharacterDeadSignal>();
            Container.DeclareSignal<GameOverSignal>();
            Container.DeclareSignal<PlayerTurnEndPossibleSignal>();
        }

    }
}