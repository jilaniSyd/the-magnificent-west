using UnityEngine;

namespace TheMagnificentWest.Components {
    public class LookAtCamera : MonoBehaviour {

        private Transform cameraTransform;

        private void Awake() {
            if (Camera.main != null) {
                cameraTransform = Camera.main.transform.parent;
            }
        }

        private void LateUpdate() {
            if (cameraTransform != null) {
                var cameraRotation = cameraTransform.rotation;
                transform.LookAt(transform.position + cameraRotation * Vector3.forward, cameraRotation * Vector3.up);
            }
        }

    }
}