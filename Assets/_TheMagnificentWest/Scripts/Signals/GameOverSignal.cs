using TheMagnificentWest.Models;

namespace TheMagnificentWest.Signals {
    public struct GameOverSignal {

        public GameTurnType GameTurnWinner { get; }

        public GameOverSignal(GameTurnType gameTurnType) {
            GameTurnWinner = gameTurnType;
        }

    }
}