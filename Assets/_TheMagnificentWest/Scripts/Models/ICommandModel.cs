using TheMagnificentWest.Characters;

namespace TheMagnificentWest.Models {
    public interface ICommandModel {

        public CharacterView Performer { get; }

    }
}