using System.Collections;
using UniRx;
using UnityEngine;

namespace TheMagnificentWest.Effects {
    public class BulletFxBehaviour : MonoBehaviour {

        [SerializeField]
        private LineRenderer lineRenderer;

        private const int WaitForFrames = 2;
        public void ShowEffect(Vector3 startPoint, Vector3 endPoint) {
            lineRenderer.SetPositions(new[] {startPoint, endPoint});
            lineRenderer.alignment = LineAlignment.View;
            StartCoroutine(HideAfterFrames(WaitForFrames));
        }

        IEnumerator HideAfterFrames(int frames) {
            yield return Observable.TimerFrame(frames).ToYieldInstruction();
            Destroy(gameObject);
        }

    }
}