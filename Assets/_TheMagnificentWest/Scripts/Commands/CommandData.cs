using TheMagnificentWest.Models;

namespace TheMagnificentWest.Commands {
    public class CommandData : ICommandData {

        public ICommand Command { get; }
        public ICommandModel CommandModel { get; }

        public CommandData(ICommand command, ICommandModel commandModel) {
            Command = command;
            CommandModel = commandModel;
        }

    }
}