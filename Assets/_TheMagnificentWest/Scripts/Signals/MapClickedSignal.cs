using UnityEngine;

namespace TheMagnificentWest.Signals {
    public struct MapClickedSignal {

        public Vector3 Point { get; }

        public MapClickedSignal(Vector3 point) {
            Point = point;
        }

    }
}