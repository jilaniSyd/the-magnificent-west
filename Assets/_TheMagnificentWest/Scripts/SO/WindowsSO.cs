﻿using System;
using System.Collections.Generic;
using TheMagnificentWest.UI;
using UnityEngine;

namespace TheMagnificentWest.Config
{
    [CreateAssetMenu(fileName = "WindowsSO", menuName = "TheMagnificentWest/ScriptableObjects/WindowSO")]
    public class WindowsSO : ScriptableObject
    {
        [SerializeField] private List<WindowView> initialWindows;

        [SerializeField] private List<WindowView> windowsData;

        public List<WindowView> InitialWindows => initialWindows;
        public List<WindowView> WindowData => windowsData;
    }
}