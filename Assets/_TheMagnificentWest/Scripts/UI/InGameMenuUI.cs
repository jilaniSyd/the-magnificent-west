using System;
using DG.Tweening;
using TheMagnificentWest.Models;
using TheMagnificentWest.Signals;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;
using UnityEngine.UI;

namespace TheMagnificentWest.UI {
    public class InGameMenuUI : WindowView {

        [SerializeField]
        private TextMeshProUGUI turnText;

        [SerializeField]
        private TextMeshProUGUI turnHighlightText;

        [SerializeField]
        private Button endTurnButton;

        [SerializeField]
        private Button replayButton;

        [SerializeField]
        private TextMeshProUGUI gameOverText;

        [SerializeField]
        private GameObject gameOverPanel;

        [SerializeField]
        private Button pauseButton;

        [Inject]
        private SignalBus signalBus;

        [Inject]
        private GamePlayModel gamePlayModel;

        [Inject]
        private IWindowSystem windowSystem;

        private IDisposable disposable;

        private void OnEnable() {
            replayButton.onClick.AddListener(ReplayGame);
            endTurnButton.onClick.AddListener(OnEndTurn);
            pauseButton.onClick.AddListener(OnPauseButton);
            disposable = gamePlayModel.Turn.Subscribe(OnTurnChanged);
            signalBus.Subscribe<GameOverSignal>(OnGameOver);
            signalBus.Subscribe<PlayerTurnEndPossibleSignal>(OnPlayerCanFinishTurn);
            gameOverPanel.SetActive(false);
            endTurnButton.interactable = false;
            turnHighlightText.alpha = 0f;
        }

        private void OnDisable() {
            pauseButton.onClick.AddListener(OnPauseButton);
            replayButton.onClick.RemoveListener(ReplayGame);
            endTurnButton.onClick.RemoveListener(OnEndTurn);
            disposable.Dispose();
            signalBus.Unsubscribe<GameOverSignal>(OnGameOver);
            signalBus.Unsubscribe<PlayerTurnEndPossibleSignal>(OnPlayerCanFinishTurn);
        }

        private void OnPauseButton() {
            windowSystem.OpenWindow<PauseMenuWindow>();
        }

        private void ReplayGame() {
            SceneManager.LoadScene("MainScene");
        }

        private void OnGameOver(GameOverSignal gameOverSignal) {
            DOVirtual.DelayedCall(1f, () => {
                gameOverPanel.SetActive(true);
                gameOverText.text = $"Game Over \n \n  {gameOverSignal.GameTurnWinner} won";
            });
        }

        private void OnEndTurn() {
            signalBus.Fire(new GameTurnChanged(GameTurnType.AI));
        }

        private void OnTurnChanged(GameTurnType gameTurnType) {
            var turnTextString = gameTurnType == GameTurnType.AI ? "Opponent" : "Player";
            turnText.text = turnTextString;
            if (gameTurnType == GameTurnType.AI) {
                endTurnButton.interactable = false;
            }

            turnHighlightText.text = "";
            turnHighlightText.DOFade(1f, 1f).OnComplete(() => {
                turnHighlightText.text = $"Now {turnTextString} turn";
                turnHighlightText.DOFade(0f, 3f);
            });
        }

        private void OnPlayerCanFinishTurn() {
            endTurnButton.interactable = true;
        }

    }
}