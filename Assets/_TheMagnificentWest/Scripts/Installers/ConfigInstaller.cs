using TheMagnificentWest.Config;
using TheMagnificentWest.SO;
using TheMagnificentWest.Systems;
using UnityEngine;
using Zenject;

namespace TheMagnificentWest.Installers {
    public class ConfigInstaller : MonoInstaller {

        [SerializeField]
        private ConfigSO configSo;

        [SerializeField]
        private WindowsSO windowsSo;

        [SerializeField]
        private CharactersSO charactersSo;

        [SerializeField]
        private UnitConfigSO unitConfigSo;

        [SerializeField]
        private SpawnPointsSO spawnPointsSo;

        [SerializeField]
        private FxSO fxSo;

        public override void InstallBindings() {
            BindConfig();
        }


        private void BindConfig() {
            Container.Bind<ConfigSO>().FromScriptableObject(configSo).AsSingle().NonLazy();
            Container.Bind<WindowsSO>().FromScriptableObject(windowsSo).AsSingle().NonLazy();
            Container.Bind<CharactersSO>().FromScriptableObject(charactersSo).AsSingle().NonLazy();
            Container.Bind<UnitConfigSO>().FromScriptableObject(unitConfigSo).AsSingle().NonLazy();
            Container.Bind<SpawnPointsSO>().FromScriptableObject(spawnPointsSo).AsSingle().NonLazy();
            Container.Bind<FxSO>().FromScriptableObject(fxSo).AsSingle().NonLazy();
        }

    }
}