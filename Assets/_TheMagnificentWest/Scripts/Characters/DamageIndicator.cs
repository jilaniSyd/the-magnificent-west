using TMPro;
using UnityEngine;

namespace TheMagnificentWest.Characters {
    public class DamageIndicator : MonoBehaviour {

        [SerializeField]
        private Animator animator;

        [SerializeField]
        private TextMeshProUGUI damageText;

        private string animationHash = "Play";

        public void ShowDamage(int damage) {
            damageText.text = $"-{damage}";
            animator.SetTrigger(animationHash);
        }

    }
}